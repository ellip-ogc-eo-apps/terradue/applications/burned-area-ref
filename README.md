## EOEPCA - Burned area by reference

### About this application

This is a simple application used as an artifact for testing EOEPCA release 0.2

It validates the fan-in without stage-in paradigm where Sentinel-2 acquisitions exposed as STAC items on Amazon are processed to the burned area using NDVI and NDWI.  

### Build the docker

The repo contains a Dockerfile and a Jenkinsfile.  

The build is done by Terradue's Jenkins instance with the configured job https://build.terradue.com/job/containers/job/eoepca-burned-area-ref/

### Create the application package

Run the command below to print the CWL: 

```bash
docker run --rm -it terradue/eoepca-burned-area-ref:0.1 burned-area-ref --docker 'docker.terradue.com/eoepca-burned-area-ref:0.1'
```

Save the CWL output to a file called `eoepca-burned-area-ref.cwl`

Package it as an application package wrapped in an Atom file with:

```bash
cwl2atom eoepca-burned-area-ref.cwl > eoepca-burned-area-ref.atom 
```

Post the Atom on the EOEPCA resource manager

### Application execution

Use the parameters:

* **pre_event**: https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2B_36RTT_20191205_0_L2A
* **post_event**:https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2B_36RTT_20191215_0_L2A
* **ndvi_threshold**: 0.19
* **ndwi_threshold**: 0.18